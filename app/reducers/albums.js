import {
     INCORRECT_ANSWER,
     REQUEST_ALBUMS,
     RECEIVE_ALBUMS,
     RESTART_APP
} from '../actions/constants';
import _ from 'lodash';

const defaultAlbumsObject = {
    albumsToShow: [],
    albums: {},
    artist: {}
};

function receiveAlbumsHelper(state, action) {
    const newState = {};
    // In iTunes api first element of album collection is artist entity
    newState.artist = action.albums[0];
    newState.albums = action.albums.slice(1);
    newState.albumsToShow = action.albums.slice(1, 2).map(album => album.collectionId);
    return newState;
}

function incorectAnswerAlbumsHelper(state) {
    const newState = {};
    newState.albumsToShow = _
        .take(state.albums, state.albumsToShow.length + 1)
        .map(album => album.collectionId);
    return {...state, ...newState};
}

export default function albums(state = defaultAlbumsObject, action) {
    switch (action.type) {
        // implement show spinner logic here
        case REQUEST_ALBUMS:
            return state;
        case RECEIVE_ALBUMS:
            return receiveAlbumsHelper(state, action);
        case INCORRECT_ANSWER:
            return incorectAnswerAlbumsHelper(state, action);
        case RESTART_APP:
            return defaultAlbumsObject;
        default:
            return state;
    }
}
