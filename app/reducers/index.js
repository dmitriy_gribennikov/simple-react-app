import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import gameProcess from './gameProcess';
import albums from './albums';
import {
     CHANGE_ARTIST_INPUT,
     RIGHT_ANSWER,
     RESTART_APP
} from '../actions/constants';

const artistInput = (state = '', action) => {
    switch (action.type) {
        case CHANGE_ARTIST_INPUT:
            return action.artistInput;
        case RESTART_APP:
            return '';
        case RIGHT_ANSWER:
            return '';
        default:
            return state;
    }
};

const rootReducer = combineReducers({
    albums,
    gameProcess,
    artistInput,
    routing,
});

export default rootReducer;
