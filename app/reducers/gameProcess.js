import {
     RIGHT_ANSWER,
     INCORRECT_ANSWER,
     RESTART_APP,
     MAX_ROUNDS,
     ATTEMPTS_TO_FAIL
} from '../actions/constants';

const defaultGameProcess = {
    round: 1,
    failedAttempts: 0,
    score: 0,
    win: false,
    lose: false
};

function scoreCalculator(attemptsCounter) {
    switch(attemptsCounter) {
        case 0:
            return 5;
        case 1:
            return 3;
        case 2:
            return 1;
        default:
            return 0;
    }
}

function rightAnswerHelper(state) {
    const newState = {};
    newState.round = state.round + 1;
    newState.failedAttempts = 0;
    newState.score = state.score + scoreCalculator(state.failedAttempts);
    newState.win = newState.round > MAX_ROUNDS ? true : false;
    newState.lose = newState.failedAttempts === ATTEMPTS_TO_FAIL ? true : false;
    return newState;
}

function incorrectAnswerHelper(state) {
    const newState = {};
    newState.round = state.round;
    newState.failedAttempts = state.failedAttempts + 1;
    newState.score = state.score;
    newState.win = newState.round > MAX_ROUNDS ? true : false;
    newState.lose = newState.failedAttempts === ATTEMPTS_TO_FAIL ? true : false;
    return newState;
}

export default function gameProcess(state = defaultGameProcess, action) {
    switch (action.type) {
        case RIGHT_ANSWER:
            return rightAnswerHelper(state);
        case INCORRECT_ANSWER:
            return incorrectAnswerHelper(state);
        case RESTART_APP:
            return defaultGameProcess;
        default:
            return state;
    }
}
