import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { changeArtistInput, rightAnswer, incorrectAnswer, restartApp, getAlbums} from '../actions';
import AlbumList from '../components/AlbumList';
import GuessArtistForm from '../components/GuessArtistForm';
import {Card, CardHeader, CardText } from 'material-ui';

const mapStateToProps = (state) => {
    return {
        artistInput: state.artistInput,
        gameProcess: state.gameProcess,
        albums: state.albums,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onChangeArtistInput: name => dispatch(changeArtistInput(name)),
        getAlbums: event => dispatch(getAlbums(event)),
        rightAnswer: () => dispatch(rightAnswer()),
        incorrectAnswer: () => dispatch(incorrectAnswer()),
        restartApp: () => dispatch(restartApp()),
    };
};

const styles = {
    containeer: {
        width: '25%',
        display: 'inline-block',
        margin: '5px',
        height: '500px',
        verticalAlign: 'top'
    },
    card: {
        height: '100%'
    },
    header: {
        fontSize: '25px',
        fontWeight: '500'
    }
};

@connect(mapStateToProps, mapDispatchToProps)
export default class Main extends Component {
    constructor(...options) {
        super(...options);
    }

    componentDidMount() {
        return this.props.getAlbums();
    }
    render() {
        const {artistInput, albums, gameProcess } = this.props;
        return (
            <div className="main-containeer">
                <div style={styles.containeer}>
                    <Card className="album-containeer" style={styles.card}>
                        <CardHeader titleStyle={styles.header} className="round-counter" title={`Round ${gameProcess.round}`}/>
                        <CardText>
                            <AlbumList albums={albums} />
                        </CardText>
                    </Card>
                </div>
                <div style={styles.containeer}>
                    <Card style={styles.card}>
                        <GuessArtistForm
                            artistNameInputHandler={(name) => this.props.onChangeArtistInput(name)}
                            artistName={artistInput}
                            gameProcess={gameProcess}
                            albums={albums}
                            incorrectAnswer={() => this.props.incorrectAnswer()}
                            rightAnswer={() => this.props.rightAnswer()}
                            getAlbums={() => this.props.getAlbums()}
                            restartApp={() => this.props.restartApp()}
                        />
                    </Card>
                </div>
            </div>
        );
    }
}

Main.propTypes = {
    artistInput: PropTypes.string,
    gameProcess: PropTypes.object,
    albums: PropTypes.object,
    onChangeArtistInput: PropTypes.func,
    getAlbums: PropTypes.func,
    rightAnswer: PropTypes.func,
    incorrectAnswer: PropTypes.func,
    restartApp: PropTypes.func
};
