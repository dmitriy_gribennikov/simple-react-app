export const artists = [
    {artistId: 909253, artistName: 'Jack Johnson'},
    {artistId: 3996865, artistName: 'Metallica'},
    {artistId: 5040714, artistName: 'AC/DC'},
    {artistId: 3296287, artistName: 'Queen'},
    {artistId: 135532, artistName: 'Deep Purple'},
    {artistId: 994656, artistName: 'Led Zeppelin'},
    {artistId: 106621, artistName: 'Guns and Roses'},
    {artistId: 112018, artistName: 'Nirvana'},
    {artistId: 889780, artistName: 'Red Hot Chili Peppers'},
    {artistId: 487143, artistName: 'Pink Floyd'}
];
