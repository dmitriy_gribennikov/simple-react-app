import { CHANGE_ARTIST_INPUT, INCORRECT_ANSWER, RIGHT_ANSWER, RESTART_APP, REQUEST_ALBUMS, RECEIVE_ALBUMS  } from './constants';
import reqwest from 'reqwest';
import _ from 'lodash';
import {artists} from '../artists';

function getRandomArtist(artistsArray) {
    // TODO: implement logic to filter artists hat already was chosen
    return _.sample(artistsArray);
}

export function changeArtistInput(artistInput) {
    return {
        type: CHANGE_ARTIST_INPUT,
        artistInput
    };
}

export function rightAnswer() {
    return {
        type: RIGHT_ANSWER
    };
}

export function incorrectAnswer() {
    return {
        type: INCORRECT_ANSWER
    };
}

export function requestAlbums(artist) {
    return {
        type: REQUEST_ALBUMS,
        artist
    };
}

export function receiveAlbums(response, artistId) {
    return {
        type: RECEIVE_ALBUMS,
        artistId,
        albums: response.results
    };
}

export function fetchAlbums(artist, beforeDispatch) {
    const artistId = artist.artistId;
    return dispatch => {
        dispatch(beforeDispatch(artist));
        return reqwest({
            type: 'jsonp',
            url: `https://itunes.apple.com/lookup?id=${artistId}&entity=album&limit=3`
        })
        .then(response => dispatch(receiveAlbums(response, artistId)));
    };
}

export function getAlbums() {
    const artist = getRandomArtist(artists);
    return dispatch => dispatch(fetchAlbums(artist, requestAlbums));
}

export function clearApp() {
    return {
        type: RESTART_APP
    };
}

export function restartApp() {
    const artist = getRandomArtist(artists);
    return dispatch => dispatch(fetchAlbums(artist, clearApp));
}
