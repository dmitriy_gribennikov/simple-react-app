export const CHANGE_ARTIST_INPUT = 'CHANGE_ARTIST_INPUT';

export const REQUEST_ALBUMS = 'REQUEST_ALBUMS';
export const RECEIVE_ALBUMS = 'RECEIVE_ALBUMS';

export const NEXT_ALBUM = 'NEXT_ALBUM';
export const SHOW_SCORE = 'SHOW_SCORE';

export const RIGHT_ANSWER = 'RIGHT_ANSWER';
export const INCORRECT_ANSWER = 'INCORRECT_ANSWER';

export const RESTART_APP = 'RESTART_APP';

export const MAX_ROUNDS = 5;
export const ATTEMPTS_TO_FAIL = 3;
export const SHOW_HINT_ON = 2;
