import React, { Component, PropTypes } from 'react';
import {Card, CardText } from 'material-ui';

export default class AlbumCard extends Component {
    constructor(...options) {
        super(...options);
    }
    render() {
        return (
            <Card className="album-card">
                <CardText>
                    {this.props.album.collectionName }
                </CardText>
            </Card>
        );
    }
}

AlbumCard.propTypes = {
    filter: PropTypes.string,
    album: PropTypes.object
};
