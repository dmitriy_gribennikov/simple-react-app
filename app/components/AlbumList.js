import React, { Component, PropTypes } from 'react';
import AlbumCard from './AlbumCard';
import _ from 'lodash';

export default class AlbumList extends Component {
    constructor(...options) {
        super(...options);
    }
    render() {
        return (
            <div>
                {
                    this.props.albums.albumsToShow.map(albumId => {
                        const album = _.find(
                            this.props.albums.albums,
                            (el) => el.collectionId === albumId
                        );
                        return <AlbumCard key={album.collectionId} album={album}/>;
                    }
                )}
            </div>
        );
    }
}

AlbumList.propTypes = {
    filter: PropTypes.string,
    albums: PropTypes.object,
};
