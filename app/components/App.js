import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { AppBar } from 'material-ui';
// layout will be here
const App = ({ children }) =>
    <div>
        <AppBar title="Guess the Artist" />
        { children }
        <footer>
            <Link to="/">Guess Artist</Link>
            <Link to="/about">About</Link>
        </footer>
    </div>;

App.propTypes = {
    children: PropTypes.object
};

export default App;
