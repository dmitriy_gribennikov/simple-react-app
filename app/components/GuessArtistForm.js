import React, { Component, PropTypes } from 'react';
import { RaisedButton, TextField, Avatar, Card, CardHeader, CardText } from 'material-ui';
import _ from 'lodash';
import {SHOW_HINT_ON} from '../actions/constants';

const styles = {
    containeer: {
        width: '25%',
        display: 'inline-block',
        margin: '5px',
        height: '500px',
        verticalAlign: 'top'
    },
    card: {
        height: '100%'
    },
    header: {
        fontSize: '25px',
        fontWeight: '500'
    },
    input: {
        width: '75%',
        height: '52px'
    },
    button: {
        width: '25%',
        display: 'inline-block'
    },
    labelStyle: {
        verticalAlign: 'middle',
    }
};

export default class GuessArtistForm extends Component {
    constructor(...options) {
        super(...options);
        this.guessButtonhandler = this.guessButtonhandler.bind(this);
    }

    guessButtonhandler() {
        const {artistName, albums} = this.props;
        const namesIdentical = artistName.trim().toLowerCase() === albums.artist.artistName.toLowerCase();
        if(namesIdentical) {
            this.props.rightAnswer();
            return this.props.getAlbums();
        }
        return this.props.incorrectAnswer();
    }

    render() {
        const { gameProcess, albums } = this.props;
        const lastShowedAlbumId = _.last(albums.albumsToShow);
        const lastShowedAlbum = _.find(albums.albums, (el) => el.collectionId === lastShowedAlbumId);
        return (
            <div>
                <Card>
                    <CardHeader style={styles.header}>
                        <div>For 5 points</div>
                        <div>Whos the artist?</div>
                    </CardHeader>
                    <CardText>
                        <TextField
                            hintText="Enter Artist Name"
                            onChange={(e) => this.props.artistNameInputHandler(e.target.value)}
                            value={this.props.artistName}
                            ref={node => {this.input = node;}}
                            style={styles.input}
                        />
                        <RaisedButton primary={true} disabled={gameProcess.lose || gameProcess.win} style={styles.button} label={'Guess'} onClick={this.guessButtonhandler}/>
                        {
                            gameProcess.failedAttempts >= SHOW_HINT_ON
                                ? <div>
                                    <div className="hint-label">Maybe some hint will help?</div>
                                    <Avatar src={lastShowedAlbum.artworkUrl100} size={100}/>
                                </div>
                                : null
                        }
                        {
                            gameProcess.win || gameProcess.lose
                                ? <div>
                                    <div>{`You ${gameProcess.win ? 'win' : 'lose'}! Your Score is: ${gameProcess.score}`}</div>
                                    <RaisedButton  primary={true} label={'Play Again'} onClick={this.props.restartApp}/>
                                </div>
                                : null
                        }
                    </CardText>
                </Card>
            </div>
        );
    }
}

GuessArtistForm.propTypes = {
    artistName: PropTypes.string,
    albums: PropTypes.object,
    gameProcess: PropTypes.object,
    artistNameInputHandler: PropTypes.func,
    rightAnswer: PropTypes.func,
    incorrectAnswer: PropTypes.func,
    getAlbums: PropTypes.func,
    restartApp: PropTypes.func,
};
