

Boilerplate source code https://github.com/jpsierens/webpack-react-redux


## Run the app

0. ```npm install```
0. ```npm start```

Once running, if you want to hide the redux dev monitor: ```CTRL+H```